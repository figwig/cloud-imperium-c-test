#version 430

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

layout(std140, binding = 1) uniform VP
{
	mat4 view;
	mat4 projection;
};

layout(location = 5)uniform mat4 model;
uniform int gridSize;

out packet
{
	vec2 UV;
 	vec3 FragNormal;
};
 
void main() 
{
	UV = texCoord *gridSize;
	FragNormal = normal;

	gl_Position = projection * view * model *vec4(position, 1.0f);
}
