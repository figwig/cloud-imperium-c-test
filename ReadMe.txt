Will Hain - 2020
Cloud Imperium Test Program - Path finding

Here is my submission for the C++ programmer's test. I have used A* for pathfinding, and threaded the demo using a JobCentre-And-Worker system. 
The demo is rendered in OpenGL, and uses Dear ImGUI for UI.

In the demo, agents are represented by an orange pentagon. Grid squares are coloured green for an origin point, red for an obstacle, 
and yellow for traversed nodes.

The setup menu allows you to set various settings and then hit go, but you can skip and go default just by pressing the space bar. Thread jobs 
and FPS are shown in windows on the left side, and you can move the camera with WASD RF.

I wrote the SimpleGLRenderer for this implementation, as it was requested I didn't use my own pre-exisiting graphics library. I decided to go 
straight to the graphics API rather than use a 3rd party middleware. The main aims for it have been to be quick to develop, simple, and lightweight. 
Had rendering been assessed, I would have probably used Vulkan, but OpenGL is much quicker for demos like these. 

This implementation uses several paradigms in order to keep the code clean, simple, and light.

*Self Registering Objects and Behaviours: Using the Self-Register pattern has allowed my simulation code to remain ultra simple. When you 
create a new GamePiece, UiElement, Behaviour, or Texture, it's constructor saves a pointer to a common static vector, either contained within 
the class itself or a seperate class. While this is slightly questionable in a larger engine or code base, I like this solution as it makes clean 
up very easy, and fulfills RAII, and keeps code clean. This explains the lack the lack of "addXObject"-type methods in my scripting. 

*Responsibility Driven Design: The design of the implmentation tries to adhere as much as possible to RDD. This limits coupling and keeps code understandable 
and upgradable. However - I've broken this slightly to reduce sheer class numbers. All three resource wrappers (Model, Texture and Shader) have static methods
to setup up/import these resources, and other static methods to store reference and cleanup that resource completly. In a larger project I'd have had a 
TextureManager, ModelManager and ShaderManager.


I've used a few open source libraries and middleware:

*OpenGL - Graphics Rendering API
*GLM - Maths library designed to play nice with OpenGL
*Glad - Extention Wrangler for OpenGL
*GLFW - window and input handling for OpenGL or Vulkan
*Moody Camel's Concurrent Lockless Queue - Used for my job centre implementation
*Dear ImGui - Immediate mode UI
*StbImage - Image Loading software
*tiny_obj_loader - Lightweight model loading software

All of the above have are either in the Common folder of the zip, or within the SimpleGlRenderer Project under the "Dependencies" filter.

I did a little profiling to help me choose between using a std::map and a std::unordered_map for the A* nodes. Most a* algorithms I looked at suggested creating 
a node for each grid square but even with the default settings, the nodes for all the agents would take up a lot of memory, and most wouldn't be used. This would 
be much worse at the 1000x1000 grids I've been using for testing. At first I used a map, checking if a node had already been looked up for a given coord- and if not, 
creating one. I noticed that unordered_map was faster and cranked up the grid size to get some actual reuslts. Here they are:

All tests performed with
*1000x1000 grid
*20 agents
*5012 obstacles

Tested on an AMD Ryzen 7 3700x @ 4.6Ghz over all 16 threads

std::map
Run 1: 2.00.89
Run 2: 2.04.47
Run 3: 4.21.38
Run 4: 1.27.20

std::unordered_map
Run 1: 0.48.64
Run 2: 0.34.25
Run 3: 0.44.85
Run 4: 0.30.48


The results showed that the unordered map was much faster, and much more reliably so, so it stayed in the implementation. If I wanted to make it even faster, I'd look 
into using a set or sorting algorithm for the open list rather than a vector, as a lot of time is spent now looking for the lowest F cost node in the open list, or I'd 
look into splitting the actual A* algorithm onto separate threads, as more often than not the simulation is left running with most threads on Idle and one or two working 
on the last agents.


I'm happy with the performance of my renderer, as it's aims were only to be quick and lightweight. If I wanted to make it any faster I'd implement instancing, as there 
are only 2 models used in the demo, so this could speed things up greatly.


Running Troubleshooting  -

The executable should just work. It doesn't rely on any DLLs. As long as the machine is 64 bit and there is a GPU that supports OpenGL 4.5 then there should not be an issue.
While I've done all I can to make sure the project is portable - I still seem to lose one or two settings when I open this project on a new machine. So if it doesn't run, 
try confirming the following -

*Make sure code standard (Project Properties->General-> C++ language ) is c++17
*Make sure the working directory (Project Properties->Debugging->Working Directory) is "$(SolutionDir)"
*Make sure you're building Debug, and x64. I've not included libs for any other configs.

I've not lost these settings when moving between machines, but if it's still not running make sure of the following - 

*The main project depends on the renderer project. Project->Project Dependencies should include SimpleGLRenderer.
*Include directories should be "$(SolutionDir)..\Common\Debug" and "$(SolutionDir)\SimpleGLRenderer"
*Library Directories should be "$(SolutionDir)..\Common\Debug" and "$(SolutionDir)\x64\Debug"
*Linker Input should be "SimpleGLRenderer.lib" and "glfw/glfw3.lib"

Thanks!