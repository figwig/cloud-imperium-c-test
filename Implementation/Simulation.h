#pragma once
#include <Texture.h>
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <concurrentqueue.h>
#include "SetupMenuUI.h"
#include <mutex>
#include <functional>

class Shader;
class GamePiece;
class Model;
class AgentBehaviour;

struct GridPos
{
	GamePiece* occupying = nullptr;
	int xCoord = 0;
	int yCoord = 0;
	bool isDestination = false;
};

struct Move
{
	glm::vec2 newPos;
	GamePiece* gp = nullptr;
	
	bool operator == (const Move& other) const
	{
		return newPos == other.newPos;
	}

	bool operator < (const Move& other) const
	{
		return std::tie(newPos.x, newPos.y) < std::tie(other.newPos.x, other.newPos.y);
	}

};

class Simulation 
{
public:
	Simulation();
	~Simulation();

	void init();
	void update();

	void updateProcess();

	void placeAtCoord(GamePiece* gp, glm::vec2 pos);

	std::vector<std::vector<GridPos>> gridEntries;

	static int GridSize;

	std::vector<Move> newMoves;


	void setupGrid(int size, int agents, int obstacles);

	moodycamel::ConcurrentQueue<std::function<void()>> endOfFrameActions;

	bool shouldQuit = false;

private:
	std::atomic_bool done = true;
	std::thread mainThread;

	float ticker = 0;

	int step = -1;

	void createGrid(int agents, int obstacles);

	void clearGrid();

	float size = 10.0f;

	bool initialized = false;
	Texture* grid = nullptr;
	Texture* bricks = nullptr;
	Texture* agentTex = nullptr;
	Texture* obstacleTex = nullptr;
	Texture* startPosTex = nullptr;
	Texture* pathTex = nullptr;

	Shader* quadShader = nullptr;
	Shader* gridShader = nullptr;

	Model* cubeModel = nullptr;
	Model* gridModel = nullptr;
	Model* pentagonModel = nullptr;

	GamePiece* gridObject = nullptr;


	std::vector<glm::vec3> gridColours;

	glm::vec2 getRandomEmptyPosition(bool forDest = false);

	std::vector<AgentBehaviour*> agentBehaviours;

	SetupMenuUI* setupMenu = nullptr;

	std::vector<GamePiece*> allObjects;
};

