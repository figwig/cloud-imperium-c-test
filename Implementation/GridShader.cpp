#include "GridShader.h"
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Simulation.h"
GridShader::GridShader()
{
	 setupShader("Shaders/grid.vert", "Shaders/grid.frag", this);
	 glUseProgram(programID);

	 locSize = glGetUniformLocation(programID, "gridSize");
	 glUniform1i(locSize, 100);
}

GridShader::~GridShader()
{
}

void GridShader::setupForObject(GamePiece* gp)
{
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(gp->getModelMatrix()));
	glUniform1i(locSize, Simulation::GridSize);
}
 