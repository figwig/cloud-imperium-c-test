#include "ObjectShader.h"
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
ObjectShader::ObjectShader()
{
	setupShader("Shaders/quad.vert", "Shaders/quad.frag", this);
}

ObjectShader::~ObjectShader()
{
}

void ObjectShader::setupForObject(GamePiece* gp)
{
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(gp->getModelMatrix()));
}
