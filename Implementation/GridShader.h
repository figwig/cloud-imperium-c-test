#pragma once
#include <Shader.h>
#include <GamePiece.h>

class GridShader:public Shader
{public:
	GridShader();
	~GridShader();


	void setupForObject(GamePiece*);
private:
	GLuint locSize = 0;
};

