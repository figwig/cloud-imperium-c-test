#include "SetupMenuUI.h"
#include <imgui.h>
#include "Simulation.h"
#include <glm-0.9.9.2/glm/glm.hpp>

void SetupMenuUI::render()
{
	ImGui::Begin("Setup Menu", &open, ImGuiWindowFlags_NoResize);//ImGuiWindowFlags_NoResize

	ImGui::Text("Change settings and press Go, or just hit \nthe spacebar to start the demo with defaults!\nHit the spacebar for each step");

	if (extendedRange)
	{
		ImGui::SliderInt("GridSize", &gridSize, 5, 3000);
		ImGui::SliderInt("Agent Count", &agentCount, 1, 1000);
		ImGui::SliderInt("Obstacle Count", &obstacleCount, 1, 50012);
	}
	else
	{
		ImGui::SliderInt("GridSize", &gridSize, 5, 300);
		ImGui::SliderInt("Agent Count", &agentCount, 1, 100);
		ImGui::SliderInt("Obstacle Count", &obstacleCount, 1, 1000);
	}

	if (ImGui::Checkbox("Allow silly mode (warning - will take some time)", &extendedRange))
	{
	}

	if (agentCount + obstacleCount > gridSize* gridSize)
	{
		ImGui::Text("Grid is too small to fit that many obstacles/agents!");
	}
	else
	{
		if (ImGui::Button("Go!"))
		{
			//Sim change grid size etc
			sim->setupGrid(gridSize, agentCount, obstacleCount);
		}
	}

	ImGui::End();
}