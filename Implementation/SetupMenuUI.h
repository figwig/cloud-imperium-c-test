#pragma once
#include <UiElement.h>
class Simulation;

class SetupMenuUI : public UiElement
{
public:
	void render();

	int gridSize = 100;
	int agentCount = 20;
	int obstacleCount = 512;
	bool open = true;

	bool extendedRange = false;
	Simulation* sim = nullptr;
};

