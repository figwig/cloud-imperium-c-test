#pragma once
#include <GamePiece.h>
#include <Time.h>
#include "Simulation.h"
#include <map>
#include <glm-0.9.9.2/glm/gtx/hash.hpp>

#define MAX_NODE_COST 100000
#define MOVE_STRAIGHT_COST 10
#define MOVE_DIAG_COST 14

namespace std
{
	bool operator < (const glm::vec2&, const glm::vec2&);
}

struct Node
{
public:
	Node()
	{
		gCost = MAX_NODE_COST;
		hCost = 0;
		fCost = 0;

		fCost = gCost + hCost;

	}

	int gCost = MAX_NODE_COST;
	int hCost;
	int fCost;
	Node* cameFrom = nullptr;
	glm::vec2 pos = glm::vec2(0);

	bool closed = false;

	bool operator < (const Node& other)const
	{
		return (pos.x < other.pos.x) && (pos.y < other.pos.y);
	}
};

class AgentBehaviour : public PieceBehaviour
{
public:
	AgentBehaviour(GamePiece* gp, Simulation* sim, glm::vec2 startingPos);
	~AgentBehaviour();

	void update();

	void findPath();

	glm::vec2 destPos;

	glm::vec2 pos = glm::vec2(0);

	bool hasValidPath = false;

	int positionInPath = 0;

	Move getNextMove();

	void executeNextMove();

	bool working = false;

	int agentID = 0;
private:
	//A reference to each grid pos - so we're not storing duplicate data and we're only creating entries for nodes we're interested in

	//std::map<glm::vec2, Node*> nodes;
	std::unordered_map<glm::vec2, Node*> nodes;
	std::vector<Node* > allNodes;

	std::vector<Node*>path;



	float ticker = 0.0f;
	Simulation* sim = nullptr;

	void calcFCostForNode(Node*);
	int calcDistance(glm::vec2 n1, glm::vec2 n2);

	Node* getNode(glm::vec2 pos);

	Node* getLowestFCostNode(std::vector<Node*>* openNodes);

	void calcPath(Node* endNode);
	std::vector<Node*> getNeighbours(Node* endNode);

	void removeNode(std::vector<Node*>* nodeList, Node* nodeToRemove);

	bool checkIfInList(std::vector<Node*>* nodeList, Node* nodeToCheck);
};

