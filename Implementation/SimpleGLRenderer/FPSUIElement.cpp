#include "FPSUIElement.h"
#include "imgui.h"
#include "Time.h"
void FPSUIElement::render()
{
	ImGuiIO& io = ImGui::GetIO();

	ImGui::SetNextWindowPos(ImVec2(20, 20), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowSize(ImVec2(500, 100), ImGuiCond_FirstUseEver);

	// Main body of the Demo window starts here.
	if (!ImGui::Begin("FPS Counter", &open, ImGuiWindowFlags_NoResize))
	{
		// Early out if the window is collapsed, as an optimization.
		ImGui::End();
	}
	else
	{
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", Time::readableDeltaTime *1000.0f, 1.0f / Time::readableDeltaTime);
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::End();
	}
}