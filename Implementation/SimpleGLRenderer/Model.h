#pragma once
#include "GladIncluder.h"
#include <string>
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/hash.hpp>
#include <map>

class GamePiece;

class Model
{
public:

	Model();
	~Model();

	GLuint vao = 0;
	static void loadModel(Model*, std::string filename);
	static void sortVAO(Model* model);

	static std::vector<Model*> allModels;
	static void cleanup();

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
	std::vector<uint32_t> indices;

	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texCoord;

		bool operator ==(const Vertex& other)const
		{
			return (position == other.position && normal == other.normal && texCoord == other.texCoord);
		}
	};

	static int ID;
	int id;

	bool operator < (const Model& other) const
	{
		return id < other.id;
	}
};

namespace std
{
	template<> struct hash<Model::Vertex> {

		size_t operator()(Model::Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.position) ^
					(hash<glm::vec3>()(vertex.normal) << 1)) >> 1) ^
					(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

