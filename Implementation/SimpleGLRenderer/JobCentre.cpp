#include "JobCentre.h"
#include <concurrentqueue.h>

JobCentre* JobCentre::thisPointer = nullptr;


JobCentre* JobCentre::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new JobCentre();
	}

	return thisPointer;
}

void JobCentre::release()
{
	if (thisPointer != nullptr)
	{
		delete thisPointer;
		thisPointer = nullptr;
	}
}

JobCentre::JobCentre()
{
	Initialize();
}

JobCentre::~JobCentre()
{
	isDone = true;

	// Ensure that all jobs are done (by joining the thread, 
	// thus waiting for all its work to be done)
	for (auto& item : workers)
	{
		item->thread.join();
		delete item;
	}
}

void JobCentre::Initialize()
{
	int supportedThreads = std::thread::hardware_concurrency() -1;

	isDone = false;

	for (size_t i = 0; i < supportedThreads; ++i)
	{

		workers.push_back(new Worker());
		//workerThreads.push_back(std::thread(&JobCentre::WorkerThread, this));
		workers[i]->jc = this;
		workers[i]->thread = std::thread(&Worker::Work, workers[i]);
	}
}

void JobCentre::QueueJob(std::string jobName, std::function<void()> function)
{
	CpuJob aJob = {};
	aJob.function = function;

	aJob.reportDone = false;
	aJob.jobName = jobName;

	locklessReadyQueue.enqueue(std::move(aJob));
}

void JobCentre::waitTillAllIdle()
{
	bool allIdle = false;
	while (!allIdle)
	{

		allIdle = true;
		int sizeOfQueue = (int)locklessReadyQueue.size_approx();

		allIdle = !(sizeOfQueue > 0);

		for (auto i : workers)
		{
			if (!i->idle)
			{
				allIdle = false;
			}
		}
		sizeOfQueue = (int)locklessReadyQueue.size_approx();

		allIdle = !(sizeOfQueue > 0);
	}

}

void Worker::Work()
{
	while (true)
	{
		// Make sure that we don't need to be done now!
		if (jc->isDone) return;

		CpuJob CurJob;
		idle = false;
		bool found = jc->locklessReadyQueue.try_dequeue(CurJob);

		if (found)
		{
			idle = false;

			jobName = CurJob.jobName;
			//Do this shit
			CurJob.function();


			idle = true;
		}
		else
		{
			idle = true;
			jobName = "Idle";

			std::this_thread::sleep_for(std::chrono::milliseconds(jc->workerWaitDelay));
		}
	}
}