#pragma once
#include "GladIncluder.h"
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include <vector>
#include "GamePiece.h"
#include "UiElement.h"

typedef std::pair<Model*, std::vector<GamePiece*>> ModelGroup;
typedef std::pair<Texture*, std::vector<ModelGroup*>> TextureGroup;
typedef std::pair<Shader*, std::vector<TextureGroup*>> ShaderGroup;


class Renderer
{
public:
	static void init();
	static void render();
	static void cleanup();

	static std::vector<GamePiece*> gamePieces;
	static std::vector<ShaderGroup*> allRenderables;
	
	static void addToRenderables(GamePiece*);
	static void removeFromRenderables(GamePiece*);

	static std::vector<UiElement*> uiElements;

	static void newFrame();

	static void renderUI();
	static void renderPieces();

	static void updateCamera();

	static GLuint vpBufferHandle;
	static const GLuint vpBufferLoc = 1;

	struct VPBuffer
	{
		glm::mat4 view;
		glm::mat4 projection;
	};

	static VPBuffer vp;

	static glm::vec3 viewPos;

	static float speed;
};

