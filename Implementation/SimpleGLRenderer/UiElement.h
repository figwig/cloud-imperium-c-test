#pragma once
class UiElement
{
public:
	UiElement();
	virtual ~UiElement();
	virtual void render() = 0;
};