#include "Time.h"
#include "GladIncluder.h"

float Time::deltaTime = 0.0f;
float Time::time = 0.0f;
float Time::readableDeltaTime = 0.0f;
float Time::ticker = 0.0f;

void Time::Tick()
{
	float newTime = glfwGetTime();

	deltaTime = newTime - time;

	time = newTime;

	ticker += deltaTime;
	if (ticker > 0.5f)
	{
		ticker = 0.0f;
		readableDeltaTime = deltaTime;
	}
}