#include "Model.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include <stdexcept>
#include <unordered_map>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>

int Model::ID = 0; 
std::vector<Model*> Model::allModels;

void Model::loadModel(Model* model, std::string filename)
{

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string warn, err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename.c_str()))
	{
		throw std::runtime_error("Couldn't load model!");
	}

	std::unordered_map<Vertex, uint32_t> uniqueVertices = { };

	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;

	for (const auto& shape : shapes)
	{
		for (const auto& index : shape.mesh.indices)
		{	
			Vertex vertex = {};

			vertex.position = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2] };
			vertex.texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1] };
		
			vertex.normal = { attrib.normals[3 * index.normal_index + 0],
				attrib.normals[3 * index.normal_index + 1],
				attrib.normals[3 * index.normal_index + 2] };


			if (uniqueVertices.count(vertex) == 0)
			{
				uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
				vertices.push_back(vertex);
			}

			indices.push_back(uniqueVertices[vertex]);
		}
	}

	for (auto x : vertices)
	{
		model->positions.push_back(x.position);
		model->normals.push_back(x.normal);
		model->texCoords.push_back(x.texCoord);
	}
	model->indices = indices;
	
	sortVAO(model);
}

void Model::sortVAO(Model* model)
{
	//Make a VAO for the model
	std::vector<float> positions;
	std::vector<float> normals;
	std::vector<float> texCoords;

	for (auto x : model->positions)
	{
		positions.push_back(x.x);
		positions.push_back(x.y);
		positions.push_back(x.z);
	}

	for (auto x : model->normals)
	{
		normals.push_back(x.x);
		normals.push_back(x.y);
		normals.push_back(x.z);
	}

	for (auto x : model->texCoords)
	{
		texCoords.push_back(x.x);
		texCoords.push_back(x.y);
	}

	GLuint indicesVBO = 0;
	GLuint texCoordsVBO = 0;
	GLuint vertsVBO = 0;
	GLuint normsVBO = 0;

	glGenVertexArrays(1, &model->vao);
	glBindVertexArray(model->vao);

	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * positions.size(), positions.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * normals.size(), normals.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &texCoordsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, texCoordsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * texCoords.size(), texCoords.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &indicesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model->indices.size() * sizeof(uint32_t), model->indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
}

Model::Model()
{
	id = ID;
	ID++;
	allModels.push_back(this);
}

Model::~Model()
{
	for (int i = 0; i < allModels.size(); i++)
	{
		if (allModels[i] == this)
		{
			allModels.erase(allModels.begin() + i);
		}
	}
}

void Model::cleanup()
{
	std::vector<Model*> models = allModels;
	for (auto x : models)
	{
		delete x;
	}

}
