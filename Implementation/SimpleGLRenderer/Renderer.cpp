#include "Renderer.h"
#include "Window.h"
#include "GladIncluder.h"
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "GamePiece.h"
#include "Model.h"
#include "Texture.h"
#include "Shader.h"
#include <iostream>
#include "Time.h"

std::vector<GamePiece*> Renderer::gamePieces;
std::vector<UiElement*> Renderer::uiElements;
glm::vec3 Renderer::viewPos;

std::vector<ShaderGroup*> Renderer::allRenderables;

Renderer::VPBuffer Renderer::vp;
GLuint Renderer::vpBufferHandle;

float Renderer::speed = 4.0f;

void Renderer::init()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(Window::window, true);
	ImGui_ImplOpenGL3_Init("#version 150");

	vp = {};
	vp.projection = glm::perspective((float)glm::radians(45.0f), (float)Window::WIDTH/ Window::HEIGHT, 0.1f, 10000.0f);

	glGenBuffers(1, &vpBufferHandle);
	glBindBuffer(GL_UNIFORM_BUFFER, vpBufferHandle);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(VPBuffer), &vp, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, vpBufferHandle);

	viewPos = glm::vec3(0, 0, -15);
	//viewPos = glm::vec3(10, 0, 9);
}

void Renderer::addToRenderables(GamePiece* gp)
{

	gamePieces.push_back(gp);

	//The procces below adds the game piece to the resource graph. The resource graph stores a reference to each state that the GPU needs to be in for a given model to be renderer. They are grouped in to shader->texture->model->object
	while (true)
	{

		for (auto shader : allRenderables)
		{
			if (shader->first != gp->program) continue;
			while (true)
			{
				for (auto texture : shader->second)
				{
					if (texture->first != gp->texture) continue;

					while (true)
					{

						for (auto model : texture->second)
						{
							if (model->first != gp->model) continue;

							model->second.push_back(gp);
							return;
						}
						//Over here? There is no entry for this model yet
						ModelGroup* mg = new ModelGroup();
						mg->first = gp->model;
						texture->second.push_back(mg);
					}
				}
				//OVer here? There is no entry for this texture yet
				TextureGroup* tg = new TextureGroup();
				tg->first = gp->texture;
				shader->second.push_back(tg);
			}

		}

		//Over here? There is no entry for that shader!
		ShaderGroup* sg = new ShaderGroup();
		sg->first = gp->program;
		allRenderables.push_back(sg);
	}
}

void Renderer::removeFromRenderables(GamePiece* gp)
{

	for (int i = 0; i < Renderer::gamePieces.size(); i++)
	{
		if (Renderer::gamePieces[i] == gp)
		{
			auto it = Renderer::gamePieces.begin();
			std::advance(it, i);
			Renderer::gamePieces.erase(it);
			break;
		}

	}

	for (auto shader : allRenderables)
	{
		if (shader->first != gp->program) continue;

		for (auto texture : shader->second)
		{
			if (texture->first != gp->texture) continue;

			for (auto model : texture->second)
			{
				if (model->first != gp->model) continue;

				int i = 0;
				for (auto object : model->second)
				{
					if (object == gp)
					{
						model->second.erase(model->second.begin() + i);
						return;
					}
					i++;
				}
			}
		}
	
	}

	std::cout << "Couldn't delete object " << std::endl;
}

void Renderer::render()
{
	updateCamera();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

	vp.view = glm::translate(glm::mat4(1), viewPos);

	//Update the View projection buffer
	glBindBuffer(GL_UNIFORM_BUFFER, vpBufferHandle);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(VPBuffer), &vp, GL_DYNAMIC_DRAW);

	renderPieces();

	renderUI();

	glfwSwapBuffers(Window::window);
}

void Renderer::cleanup()
{
	std::vector<GamePiece*> tempGamePieces = gamePieces;
	for (auto x : tempGamePieces)
	{
		delete x;
	}
	std::vector<PieceBehaviour*> tempBehaviours = PieceBehaviour::allBehaviours;
	for (auto b : tempBehaviours)
	{
		delete b;
	}
	std::vector<UiElement*> tempUI = uiElements;
	for (auto u : tempUI)
	{
		delete u;
	}

	//Resources themselves are taken care of, but these containers need deleting
	for (auto shaderGroup : allRenderables)
	{
		for (auto textureGroup : shaderGroup->second)
		{
			for (auto modelGroup : textureGroup->second)
			{
				delete modelGroup;
			}
			delete textureGroup;
		}
		
	}
}

void Renderer::newFrame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void Renderer::renderUI()
{
	for (auto x : uiElements)
	{
		x->render();
	}

	ImGui::Render();

	int display_w, display_h;
	glfwGetFramebufferSize(Window::window, &display_w, &display_h);
	glViewport(0, 0, display_w, display_h);

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

}

void Renderer::renderPieces()
{
	//for (GamePiece * x : gamePieces)
	//{

	//	glUseProgram(x->program->programID);
	//	x->program->setupForObject(x);
	//	glBindVertexArray(x->model->vao);
	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(GL_TEXTURE_2D, x->texture->texID);

	//	glDrawElements(GL_TRIANGLES, (GLsizei)x->model->indices.size(), GL_UNSIGNED_INT,(void*)0);
	//}

	glActiveTexture(GL_TEXTURE0);

	for (auto shader : allRenderables)
	{
		glUseProgram(shader->first->programID);

		for (auto texture : shader->second)
		{
			glBindTexture(GL_TEXTURE_2D, texture->first->texID);
			for (auto model : texture->second)
			{
				glBindVertexArray(model->first->vao);
				for (auto object : model->second)
				{
					shader->first->setupForObject(object);
					glDrawElements(GL_TRIANGLES, (GLsizei)model->first->indices.size(), GL_UNSIGNED_INT, (void*)0);
				}
			}
		}

	}

}

void Renderer::updateCamera()
{
	if (glfwGetKey(Window::window,GLFW_KEY_A))
	{
		viewPos += glm::vec3(Time::deltaTime * speed * glm::vec4(1, 0, 0,1));
	}

	if (glfwGetKey(Window::window, GLFW_KEY_D))
	{
		viewPos += glm::vec3(Time::deltaTime * speed * glm::vec4(-1, 0, 0,1));
	}

	if (glfwGetKey(Window::window, GLFW_KEY_W))
	{
		viewPos += glm::vec3(Time::deltaTime *speed* glm::vec4(0, 0, 1,1));
	}

	if (glfwGetKey(Window::window, GLFW_KEY_S))
	{
		viewPos += glm::vec3(Time::deltaTime * speed * glm::vec4(0, 0, -1,1));
	}
	if (glfwGetKey(Window::window, GLFW_KEY_R))
	{
		viewPos += glm::vec3(Time::deltaTime * speed * glm::vec4(0, -1, 0, 1));
	}

	if (glfwGetKey(Window::window, GLFW_KEY_F))
	{
		viewPos += glm::vec3(Time::deltaTime * speed * glm::vec4(0, 1, 0, 1));
	}

}