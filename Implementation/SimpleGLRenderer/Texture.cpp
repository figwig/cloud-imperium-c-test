#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <iostream>
#include <stdexcept>
#include "GladIncluder.h"
#include <glm-0.9.9.2/glm/glm.hpp>

std::vector<Texture*> Texture::allTextures;

Texture::Texture()
{
	allTextures.push_back(this);
}

Texture::~Texture()
{
	for (int i = 0; i < allTextures.size(); i++)
	{
		if (allTextures[i] == this)
		{
			allTextures.erase(allTextures.begin() + i);
			return;
		}
	}
}

void Texture::cleanup()
{
	std::vector<Texture*> allTexs = allTextures;

	for (auto a : allTexs)
	{
		delete a;
	}
}

void Texture::loadTexture(Texture* texture, std::string filepath)
{
	GLuint id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	float min;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
	float amount = (glm::min)(16.0f, min);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	
	// load and generate the texture
	int width, height, nrChannels;
	unsigned char* data = stbi_load(filepath.c_str(), &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

	texture->texID = id;
}
