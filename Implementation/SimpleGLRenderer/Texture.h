#pragma once
#include "GladIncluder.h"
#include <glad/glad.h>
#include <string>
#include <vector>

class Texture
{
public:
	Texture();
	~Texture();

	GLuint texID = 0;

	static void loadTexture(Texture*, std::string);

	static void cleanup();

private:
	static std::vector<Texture*> allTextures;
};

