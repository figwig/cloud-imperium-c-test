#include "GamePiece.h"
#include "Renderer.h"
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include "JobCentre.h"
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>

std::vector<PieceBehaviour*> PieceBehaviour::allBehaviours;
PieceBehaviour::PieceBehaviour(GamePiece * actOn, int syncGroup)
{ 
	host = actOn;
	allBehaviours.push_back(this);
}

PieceBehaviour::~PieceBehaviour()
{
	//Remove me from list
	int i = 0; 
	for (auto x : allBehaviours)
	{

		if (x == this)
		{
			allBehaviours.erase(allBehaviours.begin() + i); 
			return;
		}
		
	}
}

void PieceBehaviour::updateBehaviours()
{
	for (auto b : allBehaviours)
	{
		//JobCentre::singleton()->QueueJob([=] {b->update(); });
		b->update();
	}
	//JobCentre::singleton()->waitTillAllIdle();
}

GamePiece::GamePiece(Model* model, Texture* texture, Shader *program) : model(model) , texture(texture), program(program)
{
	//Renderer::gamePieces.push_back(this);

	Renderer::addToRenderables(this);

	position = glm::vec3(0, 0, -10);
	scale = glm::vec3(1);
	rotation = glm::vec3(0);
	modelMatrixDirty = true;
}

GamePiece::~GamePiece()
{
	Renderer::removeFromRenderables(this);
}


glm::mat4 GamePiece::getModelMatrix()
{
	if (modelMatrixDirty)
	{
		glm::mat4 parentMatrix = glm::mat4(1);
		if (parent != nullptr)
			parentMatrix = parent->getModelMatrix();


		modelMatrixDirty = false;

		glm::mat4 rotationMat = glm::toMat4(
			glm::quat(glm::vec3(0.0f, rotation.y, 0.0f)) *
			glm::quat(glm::vec3(rotation.x, 0.0f, 0.0f)) *
			glm::quat(glm::vec3(0.0f, 0.0f, rotation.z)));

		modelMatrix = parentMatrix * glm::translate(glm::mat4(1), position) * rotationMat * glm::scale(glm::mat4(1), scale);
	}

	return modelMatrix;
}

void GamePiece::setPos(glm::vec3 p)
{
	modelMatrixDirty = true;
	position = p;
}

void GamePiece::setRot(glm::vec3 r)
{
	rotation = r;
	modelMatrixDirty = true;
}

void GamePiece::setScale(glm::vec3 s)
{
	scale = s;
	modelMatrixDirty = true;
}

glm::vec3 GamePiece::getScale()
{
	return scale;
}

glm::vec3 GamePiece::getPos()
{
	return position;
}