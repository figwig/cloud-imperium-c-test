#pragma once
#include "GladIncluder.h"

#include "Model.h"
#include "Texture.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include <atomic>
#include <thread>


class GamePiece;

/*
A PieceBehaviour is an abstract class to define any behaviour for a piece. It also statically manages a list of all behaviours, as opposed to have a behaviour manager or every piece knowing what behaviours are linked to it:
*No unessecary classes - a behaviour manager for this level would literally be 50 lines - not nessecary
*No coupling with gamepiece - a gamepiece doesn't know if a behaviour is acting on it.
*Easy clean up

Used to update all behaviours - done in order to avoid many small classes (a lines for a behaviour manager)
*/

class PieceBehaviour
{
public:

	static std::vector<PieceBehaviour*> allBehaviours;
	static void updateBehaviours();

	PieceBehaviour(GamePiece* actOn, int syncGroup = 0);
	virtual ~PieceBehaviour();

	virtual void update() = 0;
	GamePiece* host = nullptr;
protected:



};

class Shader;

class GamePiece
{
public:
	GamePiece(Model * model, Texture * texture, Shader * program);
	~GamePiece();

	Model* model = nullptr;
	Texture* texture = nullptr;
	Shader* program = nullptr;
	glm::mat4 getModelMatrix();

	void setPos(glm::vec3);
	void setRot(glm::vec3);
	void setScale(glm::vec3);

	GamePiece* parent = nullptr;

	glm::vec3 getScale();
	glm::vec3 getPos();
private:
	bool modelMatrixDirty = true;

	glm::mat4 modelMatrix;

	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
};

