#pragma once
#include "GladIncluder.h"

class Window
{
public:
	static GLFWwindow* window;

	static void createWindowAndContext();

	static void destroyWindow();

	static int WIDTH;
	static int HEIGHT;
};

void GLAPIENTRY debugMessenger(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);