#include "UiElement.h"
#include "Renderer.h"
UiElement::UiElement()
{
	Renderer::uiElements.push_back(this);
}


UiElement::~UiElement()
{
	int i = 0;
	for (auto a : Renderer::uiElements)
	{
		if (a == this)
		{
			Renderer::uiElements.erase(Renderer::uiElements.begin() + i);
		}
		i++;
	}
}
