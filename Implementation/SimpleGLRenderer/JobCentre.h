#pragma once
#include <atomic>
#include <thread>
#include <functional>
#include "ConcurrentQueue.h"
#include <vector>

class JobCentre;

class Worker
{

public:
	std::string jobName;
private:

	bool idle = true;

	std::thread thread;

	friend class JobCentre;

	JobCentre* jc = nullptr;

	void Work();
};

struct CpuJob
{
public:
	std::function<void()> function;

	std::string jobName;

	uint32_t jobID;

	bool reportDone;
};


class JobCentre
{
public:
	static JobCentre* singleton();
	static void release();

	void QueueJob(std::string jobName, std::function<void()> function);

	void waitTillAllIdle();

private:
	friend class Worker;
	friend class JobCentreUI;
	void Initialize();
	static JobCentre* thisPointer;
	JobCentre();
	~JobCentre();

	std::vector<std::thread> workerThreads;

	std::vector<Worker*> workers;

	std::atomic<bool> isDone;

	moodycamel::ConcurrentQueue<CpuJob> locklessReadyQueue;

	int workerWaitDelay = 1;
};

