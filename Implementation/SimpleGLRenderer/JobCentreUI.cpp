#include "JobCentreUI.h"
#include "imgui.h"
#include <string>

void JobCentreUI::render()
{
	ImGui::Begin("Worker Threads");

	JobCentre* jc = JobCentre::singleton();

	ImGui::Text("Worker 0: Main Thread");
	
	for (int i = 0; i < jc->workers.size(); i++)
	{
		std::string job = jc->workers[i]->jobName;
		ImGui::Text(("Worker " + std::to_string(i+1) + ": " + job).c_str());
	}

	ImGui::End();
}