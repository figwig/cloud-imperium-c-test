﻿#include "Window.h"
#include "GladIncluder.h"
#include <iostream>

GLFWwindow* Window::window;
int Window::WIDTH = 1700;
int Window::HEIGHT = 1000;

void Window::createWindowAndContext()
{
	glfwInit();
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	window = glfwCreateWindow(WIDTH, HEIGHT, "Pathfinding Demo", nullptr, nullptr);

	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGL()) {
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		
	}

	//Turn off for better perf
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

	glDebugMessageCallback(debugMessenger, 0);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK); 
	glfwSwapInterval(0);
	glEnable(GL_DEPTH_TEST);

	//glfwSetKeyCallback(window, key_callback);
	//glfwSetCursorPosCallback(window, cursor_position_callback);
	//glfwSetMouseButtonCallback(window, mouse_button_callback);
	//glfwSetScrollCallback(window, scroll_callback);
}



void Window::destroyWindow()
{
	glfwDestroyWindow(window);
	glfwTerminate();
}

void GLAPIENTRY debugMessenger(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	if(type== GL_DEBUG_TYPE_ERROR )
		std::cout << message << std::endl;
}