#pragma once
#include <UiElement.h>
class Simulation;
class ViewUI : public UiElement
{
public:
	ViewUI() {}
	~ViewUI() {}

	void render();

private:
	Simulation* sim = nullptr;
};

