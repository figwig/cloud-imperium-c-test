#version 430


in packet
{
  vec2 UV;
  vec3 FragNormal;
};

out vec4 outFragcolor;

uniform sampler2D colourTexture;

void main() 
{
  outFragcolor = texture2D(colourTexture, UV);
}