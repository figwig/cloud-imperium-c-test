#include "Simulation.h"
#include <Shader.h>
#include <GamePiece.h>
#include "GridShader.h"
#include "ObjectShader.h"
#include <cstdlib>
#include <iostream>
#include "AgentBehaviour.h"
#include <Window.h>
#include <JobCentre.h>
#include <unordered_map>
#include <imgui.h>
#include <JobCentreUI.h>
#include "ViewUI.h"
#include <random>

int Simulation::GridSize = 100;


Simulation::Simulation()
{
	setupMenu = new SetupMenuUI();

	JobCentreUI* jUI = new JobCentreUI();

	setupMenu->sim = this;

	ViewUI* view = new ViewUI();


	grid = new Texture();
	bricks = new Texture();
	agentTex = new Texture();
	obstacleTex = new Texture();
	startPosTex = new Texture();
	pathTex = new Texture();

	Texture::loadTexture(pathTex, "Textures/path.bmp");
	Texture::loadTexture(agentTex, "Textures/agent.bmp");
	Texture::loadTexture(obstacleTex, "Textures/obstacle.bmp");
	Texture::loadTexture(grid, "Textures/gridSquare.bmp");
	Texture::loadTexture(bricks, "Textures/brickwall.jpg");
	Texture::loadTexture(startPosTex, "Textures/startpos.bmp");

	quadShader = new ObjectShader();

	gridShader = new GridShader();

	cubeModel = new Model();

	pentagonModel = new Model();


	Model::loadModel(cubeModel, "Models/cubeSimple.obj");
	Model::loadModel(pentagonModel, "Models/pentagon.obj");

	//Make the Grid Object (quick and dirty)
	gridModel = new Model();
	gridModel->indices = { 0,1,2,3,4,5 };

	gridModel->positions = { 
		glm::vec3(-1.0f,-1.0f, 0.0f), 
		glm::vec3(1.0f,1.0f, 0.0f),
		glm::vec3(-1.0f,1.0f,0.0f),
		glm::vec3(-1.0f,-1.0f, 0.0f),
		glm::vec3(1.0f,-1.0f, 0.0f),
		glm::vec3(1.0f,1.0f, 0.0f) 
	};

	gridModel->normals = { 6, glm::vec3(0) };

	//Generate the texCoords from the positions;
	for (int i = 0; i < gridModel->positions.size(); i++)
	{
		glm::vec3 x = gridModel->positions[i];
		glm::vec2 t = glm::vec2(x.x + 1.0f, x.y + 1.0f) / 2.0f;
		gridModel->texCoords.push_back(t);
	}

	Model::sortVAO(gridModel);
}

Simulation::~Simulation()
{
	//All textures, models and gamepieces are automatically deleted by renderer
	//only need cleanup for behaviour releated actions
	clearGrid();
}

void Simulation::init()
{
	gridObject = new GamePiece(gridModel, grid, gridShader);


	gridObject->setRot(glm::vec3(0, 0, 0));
	gridObject->setScale(glm::vec3(10));
}

void Simulation::update()
{
	ticker -= Time::deltaTime;

	//Set off the job
	if (glfwGetKey(Window::window, GLFW_KEY_SPACE) && ticker < 0 && done)
	{

	
		//For quick start - step -1 means no grid yet! 
		if (step == -1)
		{
			//Grid not setup yet
			setupGrid(100, 20, 512);

			step = 0;
		}

		done = false;
		//Submit Job
		JobCentre::singleton()->QueueJob("Main Pathfinding Thread",[=] {updateProcess(); done = true; });
	}

	std::function<void()> action;
	while (endOfFrameActions.try_dequeue(action))
	{
		action();
	}


	if (step < 1) return;

	for (auto a : agentBehaviours)
	{
		if (a->pos != a->destPos)
		{
			return;
		}
	}

	bool open;
	//Past this point? Simulation completed!
	ImGui::Begin("Message", &open, ImGuiWindowFlags_NoResize);
	ImGui::Text("Simulation Complete!\nHit go on the grid\nsetup menu to restart");
	if (ImGui::Button("Exit"))
	{
		shouldQuit = true;
	}
	ImGui::End();

}

void Simulation::updateProcess()
{
	//Grab a reference to the job centre
	JobCentre* jc = JobCentre::singleton();
		
	//Ticker is used to make sure that you can only press the spacebar every 0.2 seconds. This is in place as other this would run for every frame during which the space bar is down - which can be a few when running at high FPS
	ticker = 0.2f;

	//A local copy of the agents list - so we can eliminate them as we're done with each!
	std::vector<AgentBehaviour*> agents;

	//Grab the agents and put them in a local list
	for (AgentBehaviour* ab : agentBehaviours)
	{
		agents.push_back(ab);

	}

	//Step 0 is finding the original paths. This usually takes the longest as every agent needs a new path!
	if (step == 0)
	{
			
		//First time, generate some paths!
		for (auto a : agents)
		{
			a->working = true;
			//Shove the find path job into the job centre to get a worker on it
			jc->QueueJob("Agent " + std::to_string(a->agentID) + " Pathfinding",[=] {a->findPath(); a->working = false; });
		}
	}

	//Keep going till break called
	while(true)
	{
		//Make a map for each planned move by each agent
		std::map<Move, AgentBehaviour*> newMoves;

		//Wait for new moves to be calculated
		//jc->waitTillAllIdle();

		//Wait for all agents to be done
		bool working = true;
		while (working)
		{
			working = false;
			for (auto x : agents)
			{
				if (x->working)
				{
					working = true;
					std::this_thread::sleep_for(std::chrono::milliseconds(1)); 
					continue;
				}
			}
		}


		//Get each intended move, and add it to the map if the move is unique (doesn't collide with another agent's planned move)
		for (auto a : agents)
		{
			//Get next intended move
			Move move = a->getNextMove();
				
			//If this move is unique
			if (newMoves.count(move) == 0)
			{
				//Add to the map
				newMoves.insert(std::make_pair(move, a));
			}
		}
			
		//Execute each valid move, and remove that agent from the agents list so that we are left with a list of agents who need recalcing
		for (auto pair : newMoves)
		{
			//Execute move!
			pair.second->executeNextMove();
				

			//Generate a path marker (enqueued to end of frame actions)
			endOfFrameActions.enqueue([=] {
				GamePiece* pathMarker = new GamePiece(cubeModel, pathTex, quadShader);
				pathMarker->parent = gridObject;
				pathMarker->setPos(pair.second->host->getPos() + glm::vec3(0, 0, 0.0001f));
				pathMarker->setScale(glm::vec3(0.09f / GridSize, 0.09f / GridSize, 0.000001f)); 
				allObjects.push_back(pathMarker); 
				});
				
			//Add to the "all objects" list - only used for clearing the grid
		

			//find and erase the agent from the local list
			int i = 0;
			for (auto agent : agents)
			{
				if (agent == pair.second)
				{
					agents.erase(agents.begin() + i);
					break;
				}
				i++;
			}
		}

		//If we're out of agents to find paths for, leave!
		if (agents.size() == 0) break;

		//Any agents left have to regenerate paths, knowing now where new agents are
		for (auto agent : agents)
		{
			agent->working = true;
			JobCentre::singleton()->QueueJob("Agent " +std::to_string( agent->agentID) + " Pathfinding", [=] {agent->findPath(); agent->working = false; });
		}

	}
	//Step counts up 
	step++;

	done = true;

}

void Simulation::setupGrid(int size, int agents, int obstacles)
{
	//Is there enough space on the grid for all the agents and obstacles?
	if (obstacles + agents > size* size)
	{
		std::cout << "Not enough space for all agents and obstacles on grid size requested!\n";
		return;
	}


	clearGrid();

	GridSize = size;

	for (int x = 0; x < GridSize; x++)
	{
		std::vector<GridPos> row;
		for (int y = 0; y < GridSize; y++)
			row.push_back({ nullptr, x, y });

		gridEntries.push_back(row);
	}

	createGrid(agents, obstacles);

	step = 0;
}

void Simulation::clearGrid()
{
	while (!done);

	//Wait for all threads to stop!
	JobCentre::singleton()->waitTillAllIdle();

	JobCentre::singleton()->release();
	JobCentre::singleton();

	//Delete all allocated Objects
	for (auto x : allObjects)
	{
		delete x;
	}
	//And their behaviours
	for (auto x : agentBehaviours)
	{
		delete x;
	}
	//Clear our lists
	agentBehaviours.clear();
	allObjects.clear();

	gridEntries.clear();
}

void Simulation::createGrid(int agents, int obstacles)
{
	//Create Obstacles
	for (int i = 0; i < obstacles; i++)
	{
		//While this looks bad- GamePiece is self registering, so a reference to it is stored in the renderer and cleaned up at the end
		GamePiece* obstacle = new GamePiece(cubeModel, obstacleTex, quadShader);
		obstacle->setScale(glm::vec3(0.09f/GridSize, 0.09f/GridSize, 0.001f/GridSize));

		placeAtCoord(obstacle, getRandomEmptyPosition());
		allObjects.push_back(obstacle);
	}

	//Create Agents
	for (int i = 0; i < agents; i++)
	{
		GamePiece* agent = new GamePiece(pentagonModel, agentTex, quadShader);
		agent->setScale(glm::vec3(0.1f/GridSize, 0.1f/GridSize, 0.01f/GridSize));
		agent->setRot(glm::vec3(0, 0, 0));

		glm::vec2 p = getRandomEmptyPosition();
		glm::vec2 destination = getRandomEmptyPosition(true);
		placeAtCoord(agent, p);

		AgentBehaviour* ab = new AgentBehaviour(agent, this, p);
		ab->destPos = destination;
		ab->agentID = i;
		agentBehaviours.push_back(ab);

		allObjects.push_back(agent);
	}

	//Create starting position tabs
	for (AgentBehaviour* ab : agentBehaviours)
	{
		GamePiece* startPos = new GamePiece(cubeModel, startPosTex, quadShader);
		startPos->parent = gridObject;
		startPos->setPos(ab->host->getPos() + glm::vec3(0,0,0.0001f));
		startPos->setScale(glm::vec3(0.09f/GridSize, 0.09f/GridSize, 0.000001f));

		allObjects.push_back(startPos);
	}
}

void Simulation::placeAtCoord(GamePiece* gp, glm::vec2 pos)
{

	float x =pos.x - (GridSize/2);
	float y = pos.y - (GridSize/2);

	if (GridSize % 2 == 0)
	{
		x += 1.0f;
	}
	else
	{
		y -= 0.5f;
		x += 0.5f;
	}
	y += 0.05f;
	gp->parent = gridObject;
	gp->setPos(glm::vec3( (x  *2.0f / GridSize)- 1.0f/GridSize,  (y * 2.0f / GridSize), 0.0001f));

	gridEntries[pos.x][pos.y].occupying = gp;
}

glm::vec2 Simulation::getRandomEmptyPosition(bool forDest)
{
	std::random_device rd;
	std::mt19937  generator(rd());
	std::uniform_int_distribution<int> distribution(0, GridSize-1);

	for (;;)
	{
		glm::vec2 pos = {distribution(generator) ,distribution(generator) };
		if (gridEntries[(int)pos.x][(int)pos.y].occupying == nullptr)
		{
			if (forDest && gridEntries[(int)pos.x][(int)pos.y].isDestination) continue;

			return pos;
		}
	}
}