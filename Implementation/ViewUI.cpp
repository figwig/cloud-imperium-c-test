#include "ViewUI.h"
#include "Simulation.h"
#include <imgui.h>
#include <Renderer.h>

void ViewUI::render()
{
	ImGui::Begin("View Controls");
	ImGui::Text("Use WASD RF to move around!");
	ImGui::Text("Press Reset to reset the view");

	if (ImGui::Button("Reset")) Renderer::viewPos =  glm::vec3(0, 0, -15);

	ImGui::End();
}