#include "AgentBehaviour.h"
#include <iostream>
namespace std
{
	bool operator < (const glm::vec2& LHand, const glm::vec2& RHand)
	{
		if (LHand == RHand) return false;
		return std::tie(LHand.x, LHand.y) < std::tie(RHand.x, RHand.y);
	}
}


AgentBehaviour::AgentBehaviour(GamePiece* gp, Simulation* sim, glm::vec2 startingPos) : PieceBehaviour(gp,1), pos(startingPos), sim(sim)
{
	//Stationary until told to generate new path
	hasValidPath = true;
}

AgentBehaviour::~AgentBehaviour()
{
	for (auto x : allNodes)
		delete x;
}

void AgentBehaviour::update()
{

	
}

Move AgentBehaviour::getNextMove()
{
	std::cout << "Position in Path " << positionInPath << " and vector size: " << path.size() << " and mem addr " << this << std::endl;

	if (positionInPath < 0 || !hasValidPath || path.size() == 0)
	{
		return {pos, host };
	}


	glm::vec2 pos = path[positionInPath]->pos;
	return { pos, host };
}

void AgentBehaviour::executeNextMove()
{
	if (sim->gridEntries[(int)pos.x][(int)pos.y].occupying == host)
	{
		sim->gridEntries[pos.x][pos.y].occupying = nullptr;
	}

	if (positionInPath < 0 || !hasValidPath) return;
	sim->placeAtCoord(host, path[positionInPath]->pos);
	pos = path[positionInPath]->pos;
	positionInPath--;
}

void AgentBehaviour::findPath()
{
	std::vector<Node*> openList;
	std::vector<Node*> closedList;

	for (auto x : allNodes)
		delete x;

	allNodes.clear();
	nodes.clear();

	Node* startNode =  new Node();
	startNode->pos = pos;
	allNodes.push_back(startNode);

	openList.push_back(startNode);
	nodes.insert(std::make_pair(pos, startNode));

	nodes[pos]->gCost = 0;
	nodes[pos]->hCost = calcDistance(pos, destPos);
	calcFCostForNode(nodes[pos]);

	while (openList.size() > 0)
	{
		Node* currentNode = getLowestFCostNode(&openList);

		if (currentNode->pos == (destPos))
		{
			//reached finalNode
			calcPath(currentNode);
			hasValidPath = true;
			return;
		}

		//Remove node we just checked from open list
		removeNode(&openList, currentNode);
		currentNode->closed = true;
		closedList.push_back(currentNode);

		//Grab all neighbours and add them to open list
		std::vector<Node*> neighbours = getNeighbours(currentNode);

		for (auto neighbour : neighbours)
		{	
			if (checkIfInList(&closedList, neighbour)) continue;

			if (sim->gridEntries[neighbour->pos.x][neighbour->pos.y].occupying != nullptr)
			{
				closedList.push_back(neighbour);
				neighbour->gCost = 9999999;
				neighbour->closed = true;
				continue;
			}
			
			int tentGCost = currentNode->gCost + calcDistance(currentNode->pos, neighbour->pos);
			//Found a quicker way to get there!
			if (tentGCost < neighbour->gCost)
			{
				neighbour->cameFrom = currentNode;
				neighbour->gCost = tentGCost;
				neighbour->hCost = calcDistance(neighbour->pos, destPos);
				calcFCostForNode(neighbour);

				if (!checkIfInList(&openList, neighbour))
				{
					openList.push_back(neighbour);
				}
			}

			
		}
	}

	//Out of nodes on open list - no path!
	hasValidPath = false;
	std::cout << "No valid Path" << std::endl;
}

bool AgentBehaviour::checkIfInList(std::vector<Node*>* list, Node* node)
{
	if (node->closed) return true;
	
	return false;

	//if (std::find(list->begin(), list->end(), node) != list->end())
	//{
	//	return true;
	//}
	//return false;

	//for (auto a : *list)
	//{
	//	if (a == node)
	//		return true;
	//}
	//return false;
}

std::vector<Node*> AgentBehaviour::getNeighbours(Node* currentNode)
{
	std::vector<Node*> neighbours;

	if (currentNode->pos.x - 1 >= 0)
	{
		//Left
		neighbours.push_back(getNode(currentNode->pos + glm::vec2(-1, 0)));
		//Left Up
		if (currentNode->pos.y - 1 >= 0) neighbours.push_back(getNode(currentNode->pos + glm::vec2(-1, -1)));
		//Left Down
		if (currentNode->pos.y + 1 < Simulation::GridSize) neighbours.push_back(getNode(currentNode->pos + glm::vec2(-1, 1)));
	}

	if (currentNode->pos.x + 1 < Simulation::GridSize)
	{
		//right
		neighbours.push_back(getNode(currentNode->pos + glm::vec2(1, 0)));
		//Right Down
		if (currentNode->pos.y - 1 >= 0) neighbours.push_back(getNode(currentNode->pos + glm::vec2(1, -1)));
		//Right up
		if (currentNode->pos.y + 1 < Simulation::GridSize) neighbours.push_back(getNode(currentNode->pos + glm::vec2(1, 1)));
	}


	if (currentNode->pos.y - 1 >= 0)
		neighbours.push_back(getNode(currentNode->pos + glm::vec2(0, -1)));

	if (currentNode->pos.y + 1 < Simulation::GridSize)
		neighbours.push_back(getNode(currentNode->pos + glm::vec2(0, 1)));


	return neighbours;
}

void AgentBehaviour::calcFCostForNode(Node* node)
{
	node->fCost = node->gCost + node->hCost;
}

int AgentBehaviour::calcDistance(glm::vec2 n1, glm::vec2 n2)
{
	int xDist = (int)std::abs(n1.x - n2.x);
	int yDist = (int)std::abs(n1.y - n2.y);
	int remain = std::abs(xDist - yDist);

	return MOVE_DIAG_COST * glm::min(xDist, yDist) + MOVE_STRAIGHT_COST * remain;
}

//This is the biggest candidate for speeding up, as checking the map for the coord takes most of the time. However - when the grid is 400 * 400 nodes - pregenerating node objects for all of that grid would take up a huge amout of space, especially when not needed. 
Node* AgentBehaviour::getNode(glm::vec2 pos)
{
	if (nodes.count(pos) == 0)
	{
		Node* newNode = new Node();
		newNode->pos = pos;
		nodes.insert(std::make_pair(pos, newNode));
		allNodes.push_back(newNode);
		return newNode;
	}
	Node* n = nodes[pos];

	return n;
}


Node* AgentBehaviour::getLowestFCostNode(std::vector<Node*>* openNodes)
{
	Node* lowest = (*openNodes)[0];
	for (int i = 1; i < openNodes->size(); i++)
	{
		if ((*openNodes)[i]->fCost < lowest->fCost)
		{
			lowest = (*openNodes)[i];
		}
	}

	return lowest;
}

void AgentBehaviour::calcPath(Node* endNode)
{
	path.clear();

	Node* currentNode = getNode(destPos);
	path.push_back(currentNode);
	while (currentNode->cameFrom != nullptr)
	{
		path.push_back(currentNode->cameFrom);
		currentNode = currentNode->cameFrom;
	}
	positionInPath =(int)path.size() -1;
	//Path is backwards! :)
}

void AgentBehaviour::removeNode(std::vector<Node*>* nodeList, Node* nodeToRemove)
{
	for (int i = 0; i < nodeList->size(); i++)
	{
		if ((*nodeList)[i] == nodeToRemove)
		{
			nodeList->erase(nodeList->begin() + i);
			return;
		}
	}
}